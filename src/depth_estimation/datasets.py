import os
from PIL import Image
import torch
import numpy as np
from torch.utils.data import Dataset
import torchvision.transforms as T
import matplotlib.pyplot as plt


class DiodeDataset(Dataset):
    def __init__(self, path, transform=None, target_transform=None):
        self.data_dict = self._get_data_dict(path)
        self.transform = transform
        self.target_transform = target_transform
        self.min_depth = 0.1

    def _load(self, img_fname, depth_fname, mask_fname):
        image = Image.open(img_fname)
        depth = np.load(depth_fname).squeeze()
        mask = np.load(mask_fname)

        mask = mask > 0
        max_depth = min(300, np.percentile(depth, 99))
        # Weird edge case, if many 0s
        if max_depth == 0.0:
            max_depth = 0.1

        depth = np.clip(depth, self.min_depth, max_depth)
        depth = np.log(depth, where=mask)

        depth = np.ma.masked_where(~mask, depth)

        depth = np.clip(depth, 0.1, np.log(max_depth))

        if self.transform:
            image = self.transform(image)
        if self.target_transform:
            depth = self.target_transform(depth)

        return image, depth

    def _get_data_dict(self, path):
        filelist = []
        for root, dirs, files in os.walk(path):
            for file in files:
                filelist.append(os.path.join(root, file))
        filelist = sorted(filelist)
        data = {
            "image": [x for x in filelist if x.endswith(".png")],
            "depth": [x for x in filelist if x.endswith("_depth.npy")],
            "mask": [x for x in filelist if x.endswith("_depth_mask.npy")]
        }
        return data

    def __len__(self):
        return len(self.data_dict['image'])

    def __getitem__(self, idx):
        img_fname = self.data_dict['image'][idx]
        depth_fname = self.data_dict['depth'][idx]
        mask_fname = self.data_dict['mask'][idx]

        x, y = self._load(img_fname, depth_fname, mask_fname)

        return x, y

    def _get_min_and_max_value(self):
        return 0, 100

    def bin_data(self, x, num_bins=256):
        _min, _max = self._get_min_and_max_value()
        values = x.flatten()
        bin_edges = torch.linspace(_min, _max, num_bins+1)
        binned_values = torch.bucketize(values, bin_edges)
        binned_tensor = binned_values.reshape(x.shape)
        binned_tensor = binned_tensor.to(torch.float32)
        return binned_tensor


def test():
    transforms = T.Compose([
        T.Resize(256, antialias=True),
        T.CenterCrop(224),
        T.ToTensor(),
        # T.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225)),
    ])
    target_transforms = T.Compose([
        T.ToTensor(),
        T.Resize(256, antialias=True),
        T.CenterCrop(224),
    ])
    dataset = DiodeDataset(
        '/home/tobias/data/diode/train/outdoor',
        transform=transforms,
        target_transform=target_transforms
    )
    fname = '/home/tobias/data/diode/train/outdoor/scene_00007/scan_00083/00007_00083_outdoor_230_040.png'
    i = np.random.randint(0, len(dataset))
    for i in range(len(dataset)):
        x, y = dataset.__getitem__(i)

        x = x.permute(1, 2, 0)
        y = y.permute(1, 2, 0)

        # fig, axs = plt.subplots(1, 2)
        # axs[0].imshow(x)
        # axs[1].imshow(y)
        # plt.show()
        if (y.shape != (224, 224, 1)):
            print(i, y.shape, '!')


if __name__ == '__main__':
    test()

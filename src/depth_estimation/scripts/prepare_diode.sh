#!/bin/bash

data_dir=$HOME/data/diode

if [ ! -d $data_dir ]; then 
    mkdir $data_dir
fi

cd $data_dir
curl -L http://diode-dataset.s3.amazonaws.com/train.tar.gz | tar -xz -C ~/data/diode
curl -L http://diode-dataset.s3.amazonaws.com/val.tar.gz | tar -xz -C ~/data/diode

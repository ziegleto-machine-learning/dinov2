import torch
import torch.nn as nn
from torch.utils.tensorboard import SummaryWriter
from models import DinoDepthEstimator
from utils import get_diode_dataloaders, run_epoch, visualize_example
import time
import copy
import numpy as np


# --- Initialization -----------------------------------------------------
writer = SummaryWriter()
batch_size = 32
num_epochs = 100
val_frac = 1

dataloader_train, dataloader_val = \
    get_diode_dataloaders(batch_size=batch_size)

# --- Model & Training ----------------------------------------
device = torch.device("cuda") \
    if torch.cuda.is_available() \
    else torch.device("cpu")
lr = 1e-4
model = DinoDepthEstimator()
model = model.to(device)
C = nn.MSELoss()
# C = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.head.parameters(), lr=lr)
# -----------------------------------------------------------------------


# --- Initial Evaluation ------------------------------------------------
print('...Start Initial Validation Phase...')
epoch_loss, exec_time = run_epoch(
    dataloader_val,
    optimizer,
    device,
    model,
    C,
    val=True
)
writer.add_scalar(
    "Validation Loss",
    epoch_loss,
    0
)
writer.add_scalar(
    "Validation Exec Time (min/epoch)",
    exec_time/60,
    0
)
print(f'Val loss: {epoch_loss:.4f}')
# -----------------------------------------------------------------------


# --- Start Training ----------------------------------------------------
i_eval = 0
i_epoch = 0
t_training_start = time.time()
best_loss = np.inf

while True:
    i_epoch += 1
    t_epoch_start = time.time()
    print(f'Epoch {i_epoch} | {num_epochs}')
    print('-'*20)

    # --- Training Phase --------------------------------------
    print('...Start Training Phase...')
    epoch_loss, exec_time = run_epoch(
        dataloader_train,
        optimizer,
        device,
        model,
        C,
        val=False
    )
    writer.add_scalar(
        "Training Loss",
        epoch_loss,
        i_epoch
    )
    writer.add_scalar(
        "Training Exec Time (min/epoch)",
        exec_time/60,
        i_epoch
    )
    print(f'Train loss: {epoch_loss:.4f}')
    # --------------------------------------------------------

    if i_epoch % val_frac != 0:
        continue

    # --- Validation Phase -----------------------------------
    print('...Start Validation Phase...')
    i_eval += 1
    epoch_loss, exec_time = run_epoch(
        dataloader_val,
        optimizer,
        device,
        model,
        C,
        val=True
    )
    writer.add_scalar(
        "Validation Loss",
        epoch_loss,
        i_epoch
    )
    writer.add_scalar(
        "Validation Exec Time (min/epoch)",
        exec_time/60,
        i_epoch
    )
    print(f'Val loss: {epoch_loss:.4f}')
    # Visualize example
    visualize_example(model, dataloader_val, i_eval)
    # --------------------------------------------------------

    if epoch_loss < best_loss:
        print('new best performing model!')
        best_loss = epoch_loss
        best_model_wts = copy.deepcopy(model.state_dict())

    if i_epoch >= num_epochs:
        t_training = t_training_start - time.time()
        print(f'finished training after {t_training/60} [mins]')
        break
# -----------------------------------------------------------------------


# --- Save model --------------------------------------------------------
model.load_state_dict(best_model_wts)
model.eval()
torch.save(model.state_dict(), 'model.pt')
# -----------------------------------------------------------------------

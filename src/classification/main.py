import torch
from classification_data_loader import HymenOpteraDataLoader
from utils import train_model, plot_features, get_patchtokens
from models import DinoClassifier
import cv2

NUM_EPOCHS = 25
BATCH_SIZE = 4
DEVICE = torch.device(
    "cuda:0" if torch.cuda.is_available() else "cpu"
)

# Initialization
dataset = HymenOpteraDataLoader(batch_size=BATCH_SIZE)
dataloader_train, dataloader_val, classes, dataset_size_train, \
    dataset_size_val = dataset.get_data()

model = DinoClassifier()
model = model.to(DEVICE)

loss_fct = torch.nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.head.parameters(), lr=1e-4)

# model = train_model(
#    model,
#    dataloader_train,
#    dataloader_val,
#    dataset_size_train,
#    dataset_size_val,
#    optimizer,
#    DEVICE,
#    loss_fct
# )

batch = next(iter(dataloader_train))
x, y = batch
x = x[0]
x = x.unsqueeze(0).to(DEVICE)
print(x.shape)
patchtokens = get_patchtokens(model, x, DEVICE)
print(patchtokens.shape)  # 256 patchtokens 384 size

# plot_features(model, dataloader_train, DEVICE)

# Verification
# Plot something...

# Save Model?

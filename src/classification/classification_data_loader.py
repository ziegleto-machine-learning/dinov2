import torch
import torchvision
import torchvision.transforms as T
import os
import numpy as np
import matplotlib.pyplot as plt


class HymenOpteraDataset:
    def __init__(self, root_dir, img_dir, transforms, batch_size, shuffle=True, num_workers=2):
        dataset = torchvision.datasets.ImageFolder(
            root=os.path.join(root_dir, img_dir),
            transform=transforms
        )
        self.dataloader = torch.utils.data.DataLoader(
            dataset,
            batch_size=batch_size,
            shuffle=shuffle,
            num_workers=num_workers
        )
        self.dataset_len = len(dataset)
        self.classes = dataset.classes

    def __call__(self):
        return self.dataloader, self.dataset_len, self.classes


class HymenOpteraDataLoader:
    def __init__(self, root_dir='../data/hymenoptera_data/', batch_size=4):
        train_transforms = T.Compose([
            T.RandomResizedCrop(224),
            T.RandomHorizontalFlip(),
            T.ToTensor(),
            T.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ])
        val_transforms = T.Compose([
            T.Resize(256),
            T.CenterCrop(224),
            T.ToTensor(),
            T.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ])
        dataset_train = HymenOpteraDataset(
            root_dir, 'train', train_transforms, batch_size
        )
        dataset_val = HymenOpteraDataset(
            root_dir, 'val', val_transforms, batch_size
        )

        self.dataloader_train, self.dataset_size_train, self.classes_train = dataset_train()
        self.dataloader_val, self.dataset_size_val, self.classes_val = dataset_val()

    def get_data(self):
        return (
            self.dataloader_train, self.dataloader_val,
            self.classes_train, self.dataset_size_train,
            self.dataset_size_val
        )

    @staticmethod
    def imshow(batch_x, batch_y, classes):
        grid = torchvision.utils.make_grid(batch_x)
        grid = grid.numpy().transpose((1, 2, 0))
        mean = np.array([0.485, 0.456, 0.406])
        std = np.array([0.229, 0.224, 0.225])
        grid = std * grid + mean
        grid = np.clip(grid, 0, 1)
        plt.imshow(grid)
        plt.title([classes[y] for y in batch_y])
        plt.show()


def main():
    dataset = HymenOpteraDataLoader()
    dataloader_train, dataloader_val, classes, dataset_size_train, \
        dataset_size_val = dataset.get_data()
    for batch in dataloader_train:
        x, y = batch
        print(x, y)
        dataset.imshow(x, y, classes)
        break


if __name__ == '__main__':
    main()

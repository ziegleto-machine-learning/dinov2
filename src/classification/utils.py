import torch
import time
import copy
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
import numpy as np


def plot_features(model, dataloader, device):
    for batch in dataloader:
        x, y = batch
        x = x.to(device)
        image = x[0].unsqueeze(0)
    patchtokens = get_patchtokens(model, image, device)
    print(patchtokens)
    pca = PCA(n_components=3)
    pca.fit(patchtokens)
    pca_features = pca.transform(patchtokens)
    pca_features = (pca_features - pca_features.min()) / \
        (pca_features.max() - pca_features.min())
    pca_features = pca_features * 255
    plt.imshow(pca_features.reshape(16, 16, 3).astype(np.uint8))
    plt.show()


def get_patchtokens(model, img_tensor, device):
    with torch.no_grad():
        preds = model.backbone.forward_features(img_tensor)
        patchtokens = preds['x_norm_patchtokens'][0]
    return patchtokens.cpu()


def run_batch(dataloader, dataset_size, optimizer, device, model, C, val=False):
    running_loss = 0.0
    running_corrects = 0

    for batch in dataloader:
        x, y = batch
        x = x.to(device)
        y = y.to(device)

        optimizer.zero_grad()
        with torch.set_grad_enabled(not val):
            y_tild = model(x)
            _, preds = torch.max(y_tild, 1)
            loss = C(y_tild, y)

            if not val:
                loss.backward()
                optimizer.step()

        running_loss += loss.item() * x.size(0)  # loss is meaned
        running_corrects += torch.sum(preds == y.data)

    epoch_loss = running_loss / dataset_size
    epoch_acc = running_corrects.double() / dataset_size

    return epoch_loss, epoch_acc


def train_model(model, dataloader_train, dataloader_val, dataset_size_train,
                dataset_size_val, optimizer, device, loss_fct, num_epochs=25):
    start = time.time()
    best_acc = 0.0
    for epoch in range(num_epochs):
        print(f'Epoch {epoch} | {num_epochs - 1}')
        print('-' * 20)

        # Train step
        epoch_loss, epoch_acc = run_batch(
            dataloader_train, dataset_size_train, optimizer,
            device, model, loss_fct, val=False
        )
        print(f"Train Loss: {epoch_loss:.4f}, Train Acc: {epoch_acc:.4f}")

        # Val step
        epoch_loss, epoch_acc = run_batch(
            dataloader_val, dataset_size_val, optimizer,
            device, model, loss_fct, val=True
        )
        print(f"Val Loss: {epoch_loss:.4f}, Val Acc: {epoch_acc:.4f}")
        if epoch_acc > best_acc:
            best_acc = epoch_acc
            best_model_wts = copy.deepcopy(model.state_dict())
        print()

    training_time = time.time() - start
    print(f'Training time: {training_time:.4f} s')
    print(f'Best val accuracy: {best_acc:.4f}')
    model.load_state_dict(best_model_wts)

    return model

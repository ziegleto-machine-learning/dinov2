import torch
import torchvision
from torchvision.models.detection import FasterRCNN
from torchvision.models.detection.rpn import AnchorGenerator


class DinoClassifier(torch.nn.Module):
    def __init__(self):
        super(DinoClassifier, self).__init__()

        self.backbone = torch.hub.load(
            'facebookresearch/dinov2', 'dinov2_vits14')
        self.head = torch.nn.Linear(384, 2)
        self.softmax = torch.nn.Softmax(dim=1)

    def forward(self, x):
        with torch.no_grad():
            x = self.backbone(x)
        x = self.head(x)
        x = self.softmax(x)
        return x


class DinoDetector(torch.nn.Module):
    def __init__(self):
        super(DinoDetector, self).__init__()

        backbone = torch.hub.load(
            'facebookresearch/dinov2', 'dinov2_vits14')
        backbone.out_channels = 384
        anchor_generator = AnchorGenerator(sizes=((32, 64, 128, 256, 512),),
                                           aspect_ratios=((0.5, 1.0, 2.0),))
        roi_pooler = torchvision.ops.MultiScaleRoIAlign(featmap_names=['0'],
                                                        output_size=7,
                                                        sampling_ratio=2)
        self.model = FasterRCNN(backbone,
                                num_classes=2,
                                rpn_anchor_generator=anchor_generator,
                                box_roi_pool=roi_pooler)

    def forward(self, x):
        x = self.model(x)
        return x
